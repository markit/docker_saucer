# Docker Saucer
## What is it for?
This Dockerfile is for running on top of an existing image. It then fetches all
of the source packages which are licensed as GPL.

## How do I run it?
```
$image="YOUR IMAGE"
DOCKER_BUILDKIT=1 docker build . --build-arg SOURCE_IMAGE=${image} -t ${image}_sauced
```

## How do I get a tarball of all the files?
```
docker run ${image}_sauced tar -cz /sources > sources.tar.gz
```

## How does it work?
It uses https://github.com/daald/dpkg-licenses to fetch all the licenses for packages installed
in the specified image and then downloads them.
