ARG SOURCE_IMAGE=ubuntu:latest
FROM ${SOURCE_IMAGE} as source_lister
RUN sed -i '/deb-src/s/^# //' /etc/apt/sources.list
RUN sed -i '/deb-src/s/^# //' /etc/apt/sources.list.d/* || true
RUN apt-get update && apt-get install -y dpkg-dev git grep sed
WORKDIR /
RUN git clone https://github.com/daald/dpkg-licenses
RUN mkdir /sources
WORKDIR /sources
RUN /dpkg-licenses/dpkg-licenses | grep -i "[^l]gpl" | cut -f 3 -d " " | sed -e "s/:.*//" | sort | uniq > package_list.txt
RUN apt-get source $(cat package_list.txt)
RUN rm -rf *.dsc *.tar.* *.asc *.diff.*

FROM ${SOURCE_IMAGE} as sources
COPY --from=source_lister /sources /sources
